# CafeCup


---

## Easy Install

- You may have to install Python 2.7 (eg on Ubuntu 16.04+) by running `apt-get install python-minimal`
- You may also have to install build-essential and python-setuptools by running `apt-get install build-essential python-setuptools`
- Passwords for Administrator and MariaDB (root) will be asked
- MariaDB (root) password may be `password` on a fresh server
- You can then login as **Administrator** with the Administrator password

Open your Terminal and enter:

#### 1. Download the install script

For Linux:

	wget https://bitbucket.org/aaimaa/cafecup/raw/05d69a3a6f2fe5a40b6ce830e7487d11a23099c4/playbooks/install.py


#### 2. Run the install script


For production:

	sudo python install.py --production

